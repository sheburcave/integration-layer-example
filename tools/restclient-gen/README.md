# Integration Layer tools

Tool module, used to generate REST client classes from openapi definitions.

## Generating REST client

* Get openapi YML definition
* Place openapi YML definition into ```src/main/openapi``` folder
* Update ```src/main/resources/application.properties```
* Run ```mvn compile```
* Generated classes should be under ```target\generated-sources\open-api-yml```

# Links

* [Quarkus openapi generator](https://github.com/quarkiverse/quarkus-openapi-generator)
