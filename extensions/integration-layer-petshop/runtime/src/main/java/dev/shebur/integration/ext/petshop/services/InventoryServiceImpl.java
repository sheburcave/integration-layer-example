package dev.shebur.integration.ext.petshop.services;

import static dev.shebur.integration.ext.petshop.api.v1.mapper.InventoryItemMapper.toInventoryItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import dev.shebur.integration.ext.petshop.api.v1.dto.InventoryItemDTO;
import dev.shebur.integration.ext.petshop.constants.FlowStep;
import dev.shebur.integration.ext.warehouse.api.WarehouseClient;
import dev.shebur.integration.ext.warehouse.generated.InventoryItem;
import dev.shebur.integration.ext.warehouse.generated.WarehouseEchoRequest;
import dev.shebur.integration.ext.warehouse.generated.WarehouseEchoResponse;

@ApplicationScoped
public class InventoryServiceImpl implements InventoryService {

    final static Logger LOG = Logger.getLogger(InventoryServiceImpl.class);

    @ConfigProperty(name = "integration.layer.petshop.inventory.category", defaultValue = "")
    protected String defaultCategory;

    @Inject
    @RestClient
    protected WarehouseClient warehouseClient;

    /**
     * Data flow to apply inventory item update to warehouse
     */
    @Override
    public boolean importInventoryItem(final InventoryItemDTO dto, final Map<String, List<String>> errorDetails) {
        // Find if item already exists
        InventoryItem inventoryItem = findInventoryItem(dto, errorDetails);

        if (inventoryItem == null) {
            // Not found - register new ite,
            inventoryItem = createInventoryItem(dto, errorDetails);
        }

        if (inventoryItem != null) {
            // Update item
            inventoryItem = updateInventoryItem(inventoryItem, dto, errorDetails);
        } else {
            recordError(FlowStep.UPDATE_ITEM, "Failed to find or register item", errorDetails);
        }

        // if update was applied and inventoryItem is populated, assume it was successful
        return inventoryItem != null;
    }

    /**
     * Try to find inventory item
     */
    protected InventoryItem findInventoryItem(final InventoryItemDTO dto, final Map<String, List<String>> errorDetails) {
        LOG.debug("Find existing item");

        final WarehouseEchoResponse response = warehouseClient.findInventoryItem(
                createExpectedEchoData(toInventoryItem(dto))
        );

        processResponseErrors(FlowStep.FIND_EXISTING_ITEM, response.getJson(), errorDetails);

        return response.getJson().getSuccess()
            ? response.getJson().getData() : null;
    };

    /**
     * Try to create a new item on warehouse
     */
    protected InventoryItem createInventoryItem(final InventoryItemDTO dto, final Map<String, List<String>> errorDetails) {
        LOG.debug("Create new item");

        final WarehouseEchoResponse response = warehouseClient.registerInventoryItem(
                createExpectedEchoData(toInventoryItem(dto))
        );

        processResponseErrors(FlowStep.CREATE_NEW_ITEM, response.getJson(), errorDetails);

        return response.getJson().getSuccess()
                ? response.getJson().getData() : null;
    }

    /**
     * Try to update item
     */
    protected InventoryItem updateInventoryItem(final InventoryItem itemToUpdate, final InventoryItemDTO infoUpdate, final Map<String, List<String>> errorDetails) {
        LOG.debug("Update inventory item");

        if (Boolean.TRUE.equals(itemToUpdate.getLocked())) {
            // when item is locked - no name update allowed
            // Return it as a error

            recordError(FlowStep.UPDATE_ITEM, "Cannot update name of locked item", errorDetails);
        } else {
            itemToUpdate.setName(infoUpdate.getName());
        }

        // If description supplied - update it, if not - skip
        infoUpdate.getDescription().ifPresent(itemToUpdate::setDescription);

        final WarehouseEchoResponse response = warehouseClient.updateInventoryItem(
                createExpectedEchoData(itemToUpdate)
        );

        processResponseErrors(FlowStep.CREATE_NEW_ITEM, response.getJson(), errorDetails);

        return response.getJson().getSuccess()
                ? response.getJson().getData() : null;
    }

    /** Helpers */

    /**
     * Extract error information from response
     */
    private void processResponseErrors(final FlowStep step, final WarehouseEchoRequest response, final Map<String, List<String>> errorDetails) {
        if (response.getSuccess() == Boolean.FALSE) {
            recordError(step, response.getMessage(), errorDetails);
        }
    }

    /**
     * Record error message into map
     */
    private void recordError(final FlowStep step, final String errorMessage, final Map<String, List<String>> errorDetails) {
        LOG.error(String.format("{}: {}", step, errorMessage));

        final String stepName = step.name();

        if (!errorDetails.containsKey(stepName)) {
            errorDetails.put(stepName, new ArrayList<>());
        }
        errorDetails.get(stepName).add(errorMessage);
    }

    /**
     * For demo purposes this REST-client use postman-echo service. So we just send data that expect to get in return,
     * not the real warehouse api
     * @param expectedItem
     * @return
     */
    private WarehouseEchoRequest createExpectedEchoData(final InventoryItem expectedItem) {
        return new WarehouseEchoRequest()
                .success(true)
                .message("")
                .data(expectedItem);
    }
}
