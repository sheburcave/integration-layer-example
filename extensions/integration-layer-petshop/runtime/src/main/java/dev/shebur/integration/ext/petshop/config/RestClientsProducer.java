package dev.shebur.integration.ext.petshop.config;

import java.net.URI;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.RestClientBuilder;

import dev.shebur.integration.ext.warehouse.api.WarehouseClient;
import io.quarkus.runtime.util.StringUtil;
import io.vertx.ext.web.RoutingContext;

/**
 * Producer allows to create beans programmatically.
 * In case of REST clients it is possible to override baseUrl for each request,
 * for example, if exact endpoint depend on tenant
 */
public class RestClientsProducer {

    @ConfigProperty(name = "integration.layer.warehouse.base-url")
    protected String baseUrl;

    @Produces
    @RequestScoped
    public WarehouseClient warehouseClient(RoutingContext routingContext) {
        final RestClientBuilder builder = RestClientBuilder.newBuilder();

        final String tenant = routingContext.get("tenant");

        final String effectiveBaseUrl = StringUtil.isNullOrEmpty(tenant)
                ? baseUrl : baseUrl.replace("<tenant>", tenant);

        builder.baseUri(URI.create(effectiveBaseUrl));

        return builder.build(WarehouseClient.class);
    }

}
