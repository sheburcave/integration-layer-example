package dev.shebur.integration.ext.petshop.constants;

/**
 * Data processing flow steps
 */
public enum FlowStep {
    FIND_EXISTING_ITEM,
    CREATE_NEW_ITEM,
    UPDATE_ITEM
}
