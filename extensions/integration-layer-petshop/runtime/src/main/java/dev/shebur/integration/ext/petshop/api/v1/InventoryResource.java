package dev.shebur.integration.ext.petshop.api.v1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;
import org.jboss.logging.MDC;

import dev.shebur.integration.ext.petshop.api.v1.dto.InventoryItemDTO;
import dev.shebur.integration.ext.petshop.api.v1.dto.InventoryItemResponse;
import dev.shebur.integration.ext.petshop.constants.MDCNames;
import dev.shebur.integration.ext.petshop.services.InventoryService;
import io.quarkus.security.Authenticated;

@Path("/petshop/v1/inventory")
@Authenticated
public class InventoryResource {
    private static final Logger LOG = Logger.getLogger(InventoryResource.class);
    @Inject
    private InventoryService service;
    @POST
    @RolesAllowed("user")
    @Produces(MediaType.APPLICATION_JSON)
    public InventoryItemResponse postInventoryItem(@Valid InventoryItemDTO dto) {
        MDC.put(MDCNames.INVENTORY_UNIQUE_ID, dto.getUniqueId());

        LOG.debug("Post inventory item");
        final Map<String, List<String>> errorDetails = new HashMap<>();
        boolean success;
        try {
            success = service.importInventoryItem(dto, errorDetails);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        } finally {
            MDC.remove(MDCNames.INVENTORY_UNIQUE_ID);
        }

        return new InventoryItemResponse(
                success, errorDetails
        );
    }

}
