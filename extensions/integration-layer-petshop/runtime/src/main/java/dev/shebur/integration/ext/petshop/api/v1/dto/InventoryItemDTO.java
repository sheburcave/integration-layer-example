package dev.shebur.integration.ext.petshop.api.v1.dto;

import java.util.Optional;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class InventoryItemDTO {

    @NotBlank(message = "UniqueId cannot be blank")
    private final String uniqueId;

    @NotBlank(message = "Name cannot be blank")
    private final String name;

    private final Optional<String> description;

    public InventoryItemDTO(String uniqueId, String name, Optional<String> description) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.description = description;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    public Optional<String> getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "InventoryItemDTO{" +
                "uniqueId=" + uniqueId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
