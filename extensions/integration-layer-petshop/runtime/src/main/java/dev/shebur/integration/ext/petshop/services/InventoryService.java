package dev.shebur.integration.ext.petshop.services;

import java.util.List;
import java.util.Map;

import dev.shebur.integration.ext.petshop.api.v1.dto.InventoryItemDTO;

public interface InventoryService {
    boolean importInventoryItem(InventoryItemDTO dto, Map<String, List<String>> errors);
}
