package dev.shebur.integration.ext.petshop.api.v1.dto;

import java.util.List;
import java.util.Map;

import dev.shebur.integration.commons.dtos.AbstractFlowResponse;

public class InventoryItemResponse extends AbstractFlowResponse {
    public InventoryItemResponse(boolean success, Map<String, List<String>> errors) {
        super(success, errors);
    }
}
