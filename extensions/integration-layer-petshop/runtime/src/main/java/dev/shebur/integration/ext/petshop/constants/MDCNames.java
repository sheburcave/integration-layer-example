package dev.shebur.integration.ext.petshop.constants;

public final class MDCNames {
    public static final String INVENTORY_UNIQUE_ID = "inventory-unique-id";
}
