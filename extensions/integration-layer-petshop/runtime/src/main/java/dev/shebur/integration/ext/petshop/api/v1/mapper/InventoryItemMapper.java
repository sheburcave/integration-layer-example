package dev.shebur.integration.ext.petshop.api.v1.mapper;

import dev.shebur.integration.ext.petshop.api.v1.dto.InventoryItemDTO;
import dev.shebur.integration.ext.warehouse.generated.InventoryItem;

public class InventoryItemMapper {

    /**
     * Maps InventoryItemDTO to InventoryItem
     * @param dto
     * @return
     */
    public static InventoryItem toInventoryItem(final InventoryItemDTO dto) {
        return new InventoryItem()
                .uniqueId(dto.getUniqueId())
                .name(dto.getName())
                .description(dto.getDescription().orElse(null));
    }

}
