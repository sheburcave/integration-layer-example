package dev.shebur.integration.ext.petshop.api.v1;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import dev.shebur.integration.ext.petshop.api.v1.dto.InventoryItemDTO;
import dev.shebur.integration.ext.petshop.api.v1.dto.InventoryItemResponse;
import dev.shebur.integration.ext.petshop.constants.FlowStep;
import dev.shebur.integration.ext.warehouse.api.WarehouseClient;
import dev.shebur.integration.ext.warehouse.generated.InventoryItem;
import dev.shebur.integration.ext.warehouse.generated.WarehouseEchoArgs;
import dev.shebur.integration.ext.warehouse.generated.WarehouseEchoRequest;
import dev.shebur.integration.ext.warehouse.generated.WarehouseEchoResponse;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.security.TestSecurity;
import io.restassured.response.Response;
import junit.framework.Assert;

@QuarkusTest
public class InventoryResourceTest {

    protected final String inventoryItemUniqueId = "ABCDEF";
    protected InventoryItemDTO inventoryItemDTO;

    protected WarehouseEchoResponse whResponseSuccess;

    protected WarehouseEchoResponse whResponseFailed;

    @InjectMock
    @RestClient
    protected WarehouseClient warehouseClient;

    @BeforeEach
    public void beforeEach() {
        this.inventoryItemDTO = new InventoryItemDTO(
                inventoryItemUniqueId,
                "Mouse with catnip",
                Optional.of("Cats love to play with it")
        );

        this.whResponseSuccess = new WarehouseEchoResponse()
                .args(new WarehouseEchoArgs())
                .json(
                        new WarehouseEchoRequest()
                                .success(true)
                                .data(new InventoryItem().id(1L).locked(false))
                );

        this.whResponseFailed = new WarehouseEchoResponse()
                .args(new WarehouseEchoArgs())
                .json(
                        new WarehouseEchoRequest()
                                .success(false)
                                .message("Error")
                );

        Mockito.when(warehouseClient.findInventoryItem(any()))
                .thenReturn(whResponseSuccess);
        Mockito.when(warehouseClient.registerInventoryItem(any()))
                .thenReturn(whResponseSuccess);
        Mockito.when(warehouseClient.updateInventoryItem(any()))
                .thenReturn(whResponseSuccess);
    }

    @Nested
    @TestSecurity(authorizationEnabled = false)
    class DataFlow {

        /**
         * Item already exists. Should not create a new one
         */
        @Test
        public void shouldNotCreateNewItemIfExistingFound() {
            // Find succeeded
            Mockito.when(warehouseClient.findInventoryItem(any()))
                    .thenReturn(whResponseSuccess);

            // Make call
            final InventoryItemResponse response = makeCall(inventoryItemDTO)
                    .then()
                    .statusCode(200)
                    .extract()
                    .as(InventoryItemResponse.class);

            // Assert that response is successful
            Assert.assertNotNull(response);
            Assert.assertTrue(response.isSuccess());
            Assert.assertEquals(0, response.getErrors().size());

            // Verify calls to RTC client
            Mockito.verify(warehouseClient, times(1))
                    .findInventoryItem(any());
            Mockito.verify(warehouseClient, times(0)) // Do not create new one
                    .registerInventoryItem(any());
            Mockito.verify(warehouseClient, times(1))
                    .updateInventoryItem(any());
        }

        /**
         * New item, that do not exist in warehouse yet, is posted.
         * Integration layer should create new item and then update it
         */
        @Test
        public void shouldCreateNewItem() {
            // Find failed, thus should proceed to creation
            Mockito.when(warehouseClient.findInventoryItem(any()))
                    .thenReturn(whResponseFailed);

            // Make call
            final InventoryItemResponse response = makeCall(inventoryItemDTO)
                    .then()
                    .statusCode(200)
                    .extract()
                    .as(InventoryItemResponse.class);

            // Assert that response is successful
            Assert.assertNotNull(response);
            Assert.assertTrue(response.isSuccess());
            Assert.assertEquals(1, response.getErrors().size());

            // Verify calls to RTC client
            Mockito.verify(warehouseClient, times(1))
                    .findInventoryItem(any());
            Mockito.verify(warehouseClient, times(1))
                    .registerInventoryItem(any());
            Mockito.verify(warehouseClient, times(1))
                    .updateInventoryItem(any());
        }

    }

    @Nested
    @TestSecurity(authorizationEnabled = false)
    class ErrorsReporting {

        /**
         * In case of errors, happened during request processing, all of them should be reported in response
         */
        @Test
        public void shouldReportErrors() {

            Mockito.when(warehouseClient.findInventoryItem(any()))
                    .thenReturn(whResponseFailed);
            Mockito.when(warehouseClient.registerInventoryItem(any()))
                    .thenReturn(whResponseFailed);

            final InventoryItemResponse response = makeCall(inventoryItemDTO)
                    .then()
                    .statusCode(200)
                    .extract()
                    .as(InventoryItemResponse.class);

            // Assert that response is successful
            Assert.assertNotNull(response);
            Assert.assertFalse(response.isSuccess());
            Assert.assertEquals(3, response.getErrors().size());

            assertThat(
                    response.getErrors().keySet(),
                    containsInAnyOrder(
                            FlowStep.FIND_EXISTING_ITEM.name(),
                            FlowStep.CREATE_NEW_ITEM.name(),
                            FlowStep.UPDATE_ITEM.name()
                    )
            );
        }
    }

    @Nested
    class Authentication {
        /**
         * Pass valid auth with required role
         */
        @Test
        @TestSecurity(user = "emusk", roles = {"user"})
        public void shouldSucceed() {
            makeCall(inventoryItemDTO)
                    .then()
                    .statusCode(200);
        }

        /**
         * Pass invalid auth with wrong role
         */
        @Test
        @TestSecurity(user = "emusk", roles = {"non-user"})
        public void shouldFailAsNoValidRoleProvided() {
            makeCall(inventoryItemDTO)
                    .then()
                    .statusCode(403);
        }
    }

    /**
     * Helpers
     */

    private static Response makeCall(final InventoryItemDTO inventoryItemDTO) {
        return given()
                .when()
                .contentType("application/json")
                .body(inventoryItemDTO)
                .post("/petshop/v1/inventory");
    }
}
