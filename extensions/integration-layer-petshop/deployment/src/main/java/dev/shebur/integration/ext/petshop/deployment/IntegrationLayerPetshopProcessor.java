package dev.shebur.integration.ext.petshop.deployment;

import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;

class IntegrationLayerPetshopProcessor {

    private static final String FEATURE = "integration-layer-petshop";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }
}
