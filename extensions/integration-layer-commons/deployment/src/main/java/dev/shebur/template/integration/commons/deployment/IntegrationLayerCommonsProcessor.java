package dev.shebur.template.integration.commons.deployment;

import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;

class IntegrationLayerCommonsProcessor {

    private static final String FEATURE = "integration-layer-commons";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }
}
