package dev.shebur.integration.commons.dtos;

import java.util.List;
import java.util.Map;

/**
 * Common abstract base for responses on processes that involve
 * several data processing steps
 */
public abstract class AbstractFlowResponse {

    /**
     * Status of response from business processes perspective
     */
    final boolean success;

    /**
     * Error messages, grouped by data processing flow step names
     */
    final Map<String, List<String>> errors;

    public AbstractFlowResponse(boolean success, Map<String, List<String>> errors) {
        this.success = success;
        this.errors = errors;
    }

    public boolean isSuccess() {
        return success;
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }
}

