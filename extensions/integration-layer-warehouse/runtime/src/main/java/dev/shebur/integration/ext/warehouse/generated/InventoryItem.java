package dev.shebur.integration.ext.warehouse.generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryItem  {

    private Long id;
    private String uniqueId;
    private String name;
    private String description;
    private Boolean locked;
    private String category;

    /**
    * Get id
    * @return id
    **/
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    /**
     * Set id
     **/
    public void setId(Long id) {
        this.id = id;
    }

    public InventoryItem id(Long id) {
        this.id = id;
        return this;
    }

    /**
    * Get uniqueId
    * @return uniqueId
    **/
    @JsonProperty("uniqueId")
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Set uniqueId
     **/
    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public InventoryItem uniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
        return this;
    }

    /**
    * Get name
    * @return name
    **/
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * Set name
     **/
    public void setName(String name) {
        this.name = name;
    }

    public InventoryItem name(String name) {
        this.name = name;
        return this;
    }

    /**
    * Get description
    * @return description
    **/
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * Set description
     **/
    public void setDescription(String description) {
        this.description = description;
    }

    public InventoryItem description(String description) {
        this.description = description;
        return this;
    }

    /**
    * Get locked
    * @return locked
    **/
    @JsonProperty("locked")
    public Boolean getLocked() {
        return locked;
    }

    /**
     * Set locked
     **/
    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public InventoryItem locked(Boolean locked) {
        this.locked = locked;
        return this;
    }

    /**
    * Get category
    * @return category
    **/
    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    /**
     * Set category
     **/
    public void setCategory(String category) {
        this.category = category;
    }

    public InventoryItem category(String category) {
        this.category = category;
        return this;
    }

    /**
     * Create a string representation of this pojo.
     **/
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class InventoryItem {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    uniqueId: ").append(toIndentedString(uniqueId)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    locked: ").append(toIndentedString(locked)).append("\n");
        sb.append("    category: ").append(toIndentedString(category)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private static String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}