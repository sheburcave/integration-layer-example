package dev.shebur.integration.ext.warehouse.generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WarehouseEchoArgs  {

    private String tenant;

    /**
    * Get tenant
    * @return tenant
    **/
    @JsonProperty("tenant")
    public String getTenant() {
        return tenant;
    }

    /**
     * Set tenant
     **/
    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public WarehouseEchoArgs tenant(String tenant) {
        this.tenant = tenant;
        return this;
    }

    /**
     * Create a string representation of this pojo.
     **/
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class WarehouseEchoArgs {\n");

        sb.append("    tenant: ").append(toIndentedString(tenant)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private static String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}