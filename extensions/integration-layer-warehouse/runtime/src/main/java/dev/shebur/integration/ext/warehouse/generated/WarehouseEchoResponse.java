package dev.shebur.integration.ext.warehouse.generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WarehouseEchoResponse  {

    private WarehouseEchoArgs args;
    private WarehouseEchoRequest json;

    /**
    * Get args
    * @return args
    **/
    @JsonProperty("args")
    public WarehouseEchoArgs getArgs() {
        return args;
    }

    /**
     * Set args
     **/
    public void setArgs(WarehouseEchoArgs args) {
        this.args = args;
    }

    public WarehouseEchoResponse args(WarehouseEchoArgs args) {
        this.args = args;
        return this;
    }

    /**
    * Get json
    * @return json
    **/
    @JsonProperty("json")
    public WarehouseEchoRequest getJson() {
        return json;
    }

    /**
     * Set json
     **/
    public void setJson(WarehouseEchoRequest json) {
        this.json = json;
    }

    public WarehouseEchoResponse json(WarehouseEchoRequest json) {
        this.json = json;
        return this;
    }

    /**
     * Create a string representation of this pojo.
     **/
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class WarehouseEchoResponse {\n");

        sb.append("    args: ").append(toIndentedString(args)).append("\n");
        sb.append("    json: ").append(toIndentedString(json)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private static String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}