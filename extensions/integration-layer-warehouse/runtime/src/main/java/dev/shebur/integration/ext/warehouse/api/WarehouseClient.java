package dev.shebur.integration.ext.warehouse.api;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import dev.shebur.integration.ext.warehouse.generated.WarehouseEchoRequest;
import dev.shebur.integration.ext.warehouse.generated.WarehouseEchoResponse;

/**
 * Just for demo case, this client use postman-echo. Thus, all 3 methods points the same endpoint
 */
@RegisterRestClient(configKey = "warehouse-client")
@RequestScoped
public interface WarehouseClient {

    /**
     * Find inventory item
     */
    @POST
    @Path("/post")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    WarehouseEchoResponse findInventoryItem(
            WarehouseEchoRequest warehouseEchoRequest
    );

    /**
     * Register inventory item
     */
    @POST
    @Path("/post")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    WarehouseEchoResponse registerInventoryItem(
            WarehouseEchoRequest warehouseEchoRequest
    );

    /**
     * Update inventory item
     */
    @POST
    @Path("/post")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    WarehouseEchoResponse updateInventoryItem(
            WarehouseEchoRequest warehouseEchoRequest
    );
}
