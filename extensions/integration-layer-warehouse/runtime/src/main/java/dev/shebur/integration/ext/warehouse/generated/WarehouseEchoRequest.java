package dev.shebur.integration.ext.warehouse.generated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WarehouseEchoRequest  {

    private Boolean success;
    private String message;
    private InventoryItem data;

    /**
    * Get success
    * @return success
    **/
    @JsonProperty("success")
    public Boolean getSuccess() {
        return success;
    }

    /**
     * Set success
     **/
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public WarehouseEchoRequest success(Boolean success) {
        this.success = success;
        return this;
    }

    /**
    * Get message
    * @return message
    **/
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    /**
     * Set message
     **/
    public void setMessage(String message) {
        this.message = message;
    }

    public WarehouseEchoRequest message(String message) {
        this.message = message;
        return this;
    }

    /**
    * Get data
    * @return data
    **/
    @JsonProperty("data")
    public InventoryItem getData() {
        return data;
    }

    /**
     * Set data
     **/
    public void setData(InventoryItem data) {
        this.data = data;
    }

    public WarehouseEchoRequest data(InventoryItem data) {
        this.data = data;
        return this;
    }

    /**
     * Create a string representation of this pojo.
     **/
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class WarehouseEchoRequest {\n");

        sb.append("    success: ").append(toIndentedString(success)).append("\n");
        sb.append("    message: ").append(toIndentedString(message)).append("\n");
        sb.append("    data: ").append(toIndentedString(data)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private static String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}