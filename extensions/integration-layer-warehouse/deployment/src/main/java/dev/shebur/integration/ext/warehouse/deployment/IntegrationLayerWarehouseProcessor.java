package dev.shebur.integration.ext.warehouse.deployment;

import io.quarkus.deployment.annotations.BuildStep;
import io.quarkus.deployment.builditem.FeatureBuildItem;

class IntegrationLayerWarehouseProcessor {

    private static final String FEATURE = "integration-layer-warehouse";

    @BuildStep
    FeatureBuildItem feature() {
        return new FeatureBuildItem(FEATURE);
    }
}
