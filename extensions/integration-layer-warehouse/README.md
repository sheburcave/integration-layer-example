# Integration Layer: Warehouse Demo

** Demo extension **

Adds REST-client, that can be used by integration layer to pull/push data.

> **_NOTE:_**  For demo purposes this REST-client is using [Postman Echo API](https://learning.postman.com/docs/developer/echo-api/).
> All example endpoints are directing the same postman-echo endpoint.


