package dev.shebur.integration.core.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

@Path("/healthcheck")
public class HealthcheckResource {
    private static final Logger LOG = Logger.getLogger(HealthcheckResource.class);

    @ConfigProperty(name = "integration.layer.healthcheck.response", defaultValue = "")
    protected String healthCheckResponse;

    /**
     * Simple endpoint to check if service is alive
     * Returns empty string or string defined in config
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String healthcheck() {
        LOG.debug("healthcheck");
        return healthCheckResponse;
    }

    /**
     * Get version of the service
     * @return
     */
    @GET
    @Path("/version")
    @Produces(MediaType.TEXT_PLAIN)
    public String version() {
        LOG.debug("version");
        return ConfigProvider.getConfig()
                .getOptionalValue("quarkus.application.version", String.class)
                .orElse("UNKNOWN");
    }

}