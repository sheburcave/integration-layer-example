package dev.shebur.template.integration.core.resources;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class HealthcheckResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
                .when().get("/healthcheck")
                .then()
                .statusCode(200)
                .body(is("I am alive!"));
    }

}