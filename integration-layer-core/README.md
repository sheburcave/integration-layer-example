# Integration Layer Core

Core part of integration layer. On its own this module implements bare minimum.
Functionality is added using [Quarkus extensions](https://quarkus.io/guides/writing-extensions)

