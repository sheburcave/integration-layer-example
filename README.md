# Integration Layer Example

Example of integration layer, built on Quarkus. Includes main _core_ module, that implements bare minimum. Additional 
resources (modules, implementing custom endpoints and corresponding logic) are implemented as extensions.

## Authentication

Exposed endpoints can be protected with authorization against IAM, supporting oauth2 flow (for example, Keycloak).

## Initiating new extension

Quarkus extension project consists of 2 separate parts - runtime (exactly needed functionality) and deployment (used by
Quarkus in build time to inject extension). Runtime part artifact will be equal to the name of extension. Deployment
will have suffix '-deployment'. See [Quarkus - Writing your own extension](https://quarkus.io/guides/writing-extensions).
It is possible to use normal java dependencies. However, extensions result in more optimized code...
as Quarkus documentation states.

Initiate new extension:

```shell
mvn io.quarkus.platform:quarkus-maven-plugin:2.16.6.Final:create-extension -N \
    -DextensionId="integration-layer-petshop" \
    -DextensionName="Petshop Integration" \
    -DextensionDescription="Demo extension, implementing endpoints for Petshop"
```

mvn io.quarkus.platform:quarkus-maven-plugin:2.16.6.Final:create-extension -N -DextensionId=integration \
-DextensionName="My Extension" \
-DextensionDescription="Do something useful."

This command will start interactive wizard.

### Post-wizard steps

* Add your extension into parent project modules list in [integration-layer/pom.xml](pom.xml)
* Add **RUNTIME** library to dependencies of [integration-layer-core/pom.xml](integration-layer-core/pom.xml)
* ***IMPORTANT*** Add build stage for _jandex-maven-plugin_ into **RUNTIME** part of extension. It is needed to make
  extension endpoints registered. Build stage plugin:

```xml

<plugin>
    <!-- https://github.com/wildfly/jandex-maven-plugin -->
    <groupId>org.jboss.jandex</groupId>
    <artifactId>jandex-maven-plugin</artifactId>
    <version>${maven.jandex.version}</version>
    <executions>
        <execution>
            <id>make-index</id>
            <goals>
                <!-- phase is 'process-classes by default' -->
                <goal>jandex</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

## Debugging

Running project with maven

```shell
./mvnw compile quarkus:dev
```

To start project with IntelliJ IDE, create debug configuration with type _Quarkus_ for module
[integration-layer-core](integration-layer-core).

Starting application in DEV-mode will also spawn Keycloak docker container, that will be used to authenticate requests.
Keycloak port changes from start to start. To find the effective one - check ```docker ps```. Or open Quarkus
development
console [http:\\localhost:8080\q\dev](http:\\localhost:8080\q\dev) and proceed to Keycloak -> Keycloak Admin.

### Starting long-running Keycloak

If quarkus can find a running container with label (not name!) 'quarkus-dev-service-keycloak', it will try to use that
one, instead of
starting a new container. That simplifies debugging as port won't be changing each time debug is ran..
Container can be started with docker (or podman):

```shell
 docker run -d -p 18080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin --label quarkus-dev-service-keycloak="quarkus" --restart "unless-stopped" quay.io/keycloak/keycloak:20.0.5 start-dev
```

```shell
 podman run -d -p 18080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin --label quarkus-dev-service-keycloak="quarkus" --restart "unless-stopped" quay.io/keycloak/keycloak:20.0.5 start-dev
```

This will make keycloak available on localhost:18080

After starting own container, import dev realm into Keayloack
from ```integration-layer-core/config/quarkus-realm.json```

### Configuring Postman

Easiest way to test endpoints is to configure Postman.
When integration-layer is running its openapi can be accessed on ```http:\\localhost:8080\openapi```
Import that one into postman and configure oauth.

## Building native image

If GraalVM with native-image is configured:

```shell
mvn package -Pnative
```

Alternatively, use docker container to build native application

```shell
docker run -v .:/opt/src -w=/opt/src vegardit/graalvm-maven:22.3.1-java11 mvn package -Pnative -DskipTests=true
```

or with podman

```shell
podman run -v .:/opt/src -w=/opt/src vegardit/graalvm-maven:22.3.1-java11 mvn package -Pnative -DskipTests=true
```

or with Maven

```shell
./mvnw install -Dnative -DskipTests -Dquarkus.native.container-build=true
```

Command above should produce an artifact ```integration-layer-core/target/*-runner```. After getting the artifact, build
docker container

```shell
[podman|docker] build -t <tag> .
```

# Links
